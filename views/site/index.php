<?php

    use yii\grid\GridView;
    use yii\helpers\Html;

    $this->title = 'Víctor Yii Exam';
    
?>

<div class="site-index">

    <div class="body-content">
        <h4><?=$enunciado1?></h4>
        <?=
            GridView::widget([
                'dataProvider' => $consulta1,
                'columns' => $campos1
            ]);   
        ?>
        </br>
        <h4><?=$enunciado2?></h4>
        <?=
            GridView::widget([
                'dataProvider' => $consulta2,
                'columns' => $campos2
            ]);   
        ?>
        </br>
        <h4><?=$enunciado3?></h4>
        <?=
            GridView::widget([
                'dataProvider' => $consulta3,
                'columns' => $campos3
            ]);   
        ?>
    </div>

</div>
