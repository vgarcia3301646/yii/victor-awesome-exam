<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        
        $consulta1 = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Ciclista::find()->select("dorsal, nombre")->distinct()->where("edad between 20 and 40"),
        ]);
        
        $consulta2 = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Etapa::find()->select("numetapa as etapa, kms as kilometros_de_longitud")->distinct()->where("salida = llegada"),
        ]);
        
        $consulta3 = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Puerto::find()->select("nompuerto as puerto, dorsal")->distinct()->where("altura > 1000"),
        ]);
        
        return $this->render('index', [
            'consulta1' => $consulta1,
            'campos1' => ['dorsal', 'nombre'],
            'enunciado1' => 'Los ciclistas cuya edad está entre 20 y 40 años mostrando únicamente el dorsal y el nombre del ciclista',
            'consulta2' => $consulta2,
            'campos2' => ['etapa', 'kilometros_de_longitud'],
            'enunciado2' => 'Las etapas circulares mostrando sólo el número de etapa y la longitud de las mismas',
            'consulta3' => $consulta3,
            'campos3' => ['puerto', 'dorsal'],
            'enunciado3' => 'Los puertos con altura mayor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó',
        ]);
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
}
